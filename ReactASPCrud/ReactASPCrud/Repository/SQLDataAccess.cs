﻿using Microsoft.Extensions.Configuration;
using ReactASPCrud.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace ReactASPCrud.Repository
{
    public class SQLDataAccess
    {
        private readonly IConfiguration _configuration;

        public SQLDataAccess(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void InsertUser(string name, string password, string email, string document)
        {

        }

        public void Update(string name, string password, string email, string document)
        { }

        public void Delete(int Id)
        { }

        /// <summary>
        /// Retrieve the Users Password from the Data Base
        /// </summary>
        /// <param name="userName"></param>
        /// <returns>The password</returns>
        public string GetUserPassword(string userName)
        {
            string pass = "";
            using (SqlConnection connection = new SqlConnection(_configuration["ConnectionString"]))
            {
                connection.Open();

                String sql = @$"SELECT Password FROM Users WHERE Email = '{userName}'";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            pass = reader["Password"].ToString();
                        }
                    }
                }
            }
            return pass;
        }

        /// <summary>
        /// Connect to DataBase for retrieve all Users
        /// </summary>
        /// <returns></returns>
        public List<User> SelectAllUsers()
        {
            List<User> users = new List<User>();
            using (SqlConnection connection = new SqlConnection(_configuration["ConnectionString"]))
            {
                connection.Open();

                String sql = "SELECT Id, Name, Email, Document, Phone FROM Users";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            users.Add(new User
                            {
                                Name = reader["Name"].ToString(),
                                Id = int.Parse(reader["Id"].ToString()),
                                Email = reader["Email"].ToString(),
                                Document = reader["Document"].ToString(),
                                Phone = reader["Phone"].ToString()
                            });
                        }
                    }
                }
            }
            return users;
        }
    }
}
