using ReactASPCrud.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using ReactASPCrud.Auth;
using ReactASPCrud.Repository;

namespace ReactASPCrud.Services
{
    public class UserService
    {
        private static List<User> users = new List<User>();
        private static int Count = 1;
        private SQLDataAccess _sql;

        private static readonly string[] names = new string[] { "Jonathan", "Mary", "Susan", "Joe", "Paul", "Carl", "Amanda", "Neil" };
        private static readonly string[] surnames = new string[] { "Smith", "O'Neil", "MacDonald", "Bailee", "Saigan", "Strip", "Spenser" };
        private static readonly string[] extensions = new string[] { "@gmail.com", "@hotmail.com", "@outlook.com", "@icloud.com", "@yahoo.com" };

        public UserService(SQLDataAccess sql)
        {
            _sql = sql;
            Random rnd = new Random();
            for (int i = 0; i < 5; i++)
            {
                string currName = names[rnd.Next(names.Length)];
                User user = new User
                {
                    Id = Count++,
                    Name = currName + " " + surnames[rnd.Next(surnames.Length)],
                    Email = currName.ToLower() + extensions[rnd.Next(extensions.Length)],
                    Document = (rnd.Next(0, 100000) * rnd.Next(0, 100000)).ToString().PadLeft(10, '0'),
                    Phone = "+1 888-452-1232"
                };

                users.Add(user);
            }
        }

        /// <summary>
        /// Get all the users recorded into the DataBase
        /// </summary>
        /// <returns>List with all Users</returns>
        public List<User> GetAll()
        {
            return _sql.SelectAllUsers();
        }

        public User GetById(int id)
        {
            return users.Where(user => user.Id == id).FirstOrDefault();
        }

        public User Create(User user)
        {
            user.Id = Count++;
            users.Add(user);

            return user;
        }

        public void Update(int id, User user)
        {
            User found = users.Where(n => n.Id == id).FirstOrDefault();
            found.Name = user.Name;
            found.Email = user.Email;
            found.Document = user.Document;
            found.Phone = user.Phone;
        }

        public void Delete(int id)
        {
            users.RemoveAll(n => n.Id == id);
        }

        /// <summary>
        /// Basic User Validation
        /// Validate the password and return JWT if is correct
        /// </summary>
        /// <param name="userName">Email Attribute</param>
        /// <param name="password">Password</param>
        /// <returns>AuthResponse with the Toke, StatusCode and Custom Message</returns>
        public AuthResponse ValidateUser(string userName, string password)
        {
            string tempPassword = _sql.GetUserPassword(userName);
            AuthResponse resp = new AuthResponse();
            if (tempPassword == password)
            {
                resp.Token = MyJsonWebToken.GetJWToken(userName);
                resp.Message = "Authentication Succesfully";
                resp.StatusCode = 200;
            }          
            return resp;
        }

    }
}
