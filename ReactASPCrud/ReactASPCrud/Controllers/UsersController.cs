﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using ReactASPCrud.Models;
using ReactASPCrud.Services;

namespace ReactASPCrud.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("ReactPolicy")]
    public class UsersController : ControllerBase
    {
        private readonly UserService userService;

        public UsersController(UserService userService)
        {
            this.userService = userService;
        }

        // GET api/users
        [HttpGet]
        [Authorize]
        public IEnumerable<User> Get()
        {
            return userService.GetAll();
        }

        // GET api/users/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<IActionResult> Get(int id)
        {
            return Ok(userService.GetById(id));
        }

        // POST api/users
        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Post([FromBody] User user)
        {
            return CreatedAtAction("Get", new { id = user.Id }, userService.Create(user));
        }

        // POST api/users/authenticate
        [HttpPost("authenticate")]
        public IActionResult Authenticate([FromBody] AuthRequest req)
        {
            AuthResponse resp = userService.ValidateUser(req.UserName, req.Password);
            if (resp.StatusCode == 200)
            {
                return Ok(resp);
            }
            else
            {
                return BadRequest(resp);
            }
        }

        // PUT api/users/5
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> Put(int id, [FromBody] User user)
        {
            userService.Update(id, user);

            return NoContent();
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> Delete(int id)
        {
            userService.Delete(id);

            return NoContent();
        }

        public override NoContentResult NoContent()
        {
            return base.NoContent();
        }
    }
}
